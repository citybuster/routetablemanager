#include "dialog.h"
#include <QApplication>

#include <QTimer>

#include "nethelper.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(":/whitix.png"));

    Dialog d;
    d.show();

    return a.exec();
}
