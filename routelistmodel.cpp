#include "routelistmodel.h"

#include "dict.h"
#include "nethelper.h"
#include "utility.h"

#include <QFile>
#include <QRegExp>

XRouteListModel::XRouteListModel(QObject *parent) :
    QAbstractItemModel(parent)
{
}

QModelIndex XRouteListModel::index(int row, int column, const QModelIndex &/*parent*/) const
{
    return createIndex(row, column);
}

QModelIndex XRouteListModel::parent(const QModelIndex &/*child*/) const
{
    return QModelIndex();
}

int XRouteListModel::rowCount(const QModelIndex &/*parent*/) const
{
    return m_Data.size();
}

int XRouteListModel::columnCount(const QModelIndex &/*parent*/) const
{
    return m_Header.size();
}

QVariant XRouteListModel::data(const QModelIndex &index, int role) const
{
    int idx = ((Cell&)m_Header[index.column()])[DatIndexRole].toInt();
    if ( idx == -1 )
    {
        QString szColName = ((Cell&)m_Header[index.column()])[Qt::DisplayRole].toString();
        return ((Cell&)((Param&)m_Param)[szColName])[role];
    }

    return ((Cell&)m_Data[index.row()][idx])[role];
}

QVariant XRouteListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal)
        return ((Cell&)m_Header[section])[role];
    if (role == Qt::DisplayRole) return section + 1;
    return QVariant();
}

void XRouteListModel::SetGateway(QString szGateway)
{
    m_Param[XDict::Gateway][Qt::DisplayRole] = szGateway;
    auto hIt = std::find_if(m_Header.begin(), m_Header.end(), [](Cell &cell){
        return cell[Qt::DisplayRole] == XDict::Gateway;
    });
    if (hIt == m_Header.end()) return;
    int Id = (*hIt)[ColIndexRole].toInt();
    emit dataChanged(index(0, Id, QModelIndex()), index(m_Data.size()-1, Id, QModelIndex()), QVector<int>(1, Qt::DisplayRole));
}

void XRouteListModel::SetMetric(QString szMetric)
{
    m_Param[XDict::Metric][Qt::DisplayRole] = szMetric;
    auto hIt = std::find_if(m_Header.begin(), m_Header.end(), [](Cell &cell){
        return cell[Qt::DisplayRole] == XDict::Metric;
    });
    if (hIt == m_Header.end()) return;
    int Id = (*hIt)[ColIndexRole].toInt();
    emit dataChanged(index(0, Id, QModelIndex()), index(m_Data.size() - 1, Id, QModelIndex()), QVector<int>(1, Qt::DisplayRole));
}

void XRouteListModel::SetInterface(QString szIntf)
{
    NetHelper helper;
    std::string intfName = helper.GetIdToStrMap(InterfaceName)[XUtility::strtoV<unsigned int>(szIntf.toStdString())];
    m_Param[XDict::Interface][Qt::DisplayRole] = intfName.c_str();
    m_Param[XDict::Interface][Qt::UserRole] = szIntf;
    auto hIt = std::find_if(m_Header.begin(), m_Header.end(), [](Cell &cell){
        return cell[Qt::DisplayRole] == XDict::Metric;
    });
    if (hIt == m_Header.end()) return;
    int Id = (*hIt)[ColIndexRole].toInt();
    emit dataChanged(index(0, Id, QModelIndex()), index(m_Data.size() - 1, Id, QModelIndex()), QVector<int>(1, Qt::DisplayRole));
}

bool XRouteListModel::Load(QString szFile)
{
    auto AddHeader = [this](int idx, QString szColName){
        auto hIt = std::find_if(m_Header.begin(), m_Header.end(), [this, &szColName](Cell &cell){
            return cell[Qt::DisplayRole] == szColName;
        });
        if (hIt == m_Header.end())
        {
            Cell cell;
            cell[Qt::DisplayRole] = szColName;
            cell[ColIndexRole] = m_Header.size();
            cell[DatIndexRole] = idx;
            m_Header.push_back(cell);
        }
    };

    auto AddCell = [this, &AddHeader](Row &row, QString szColName, QString val){
        if (val.isEmpty()) { AddHeader(-1, szColName); return ; }
        AddHeader(row.size(), szColName);

        Cell cell;
        cell[Qt::DisplayRole] = val;
        row.push_back(cell);
    };

    auto AddRow = [this, &AddCell](QString szDest, QString szMask){
        Row row;
        AddCell(row, XDict::Destination, szDest);
        AddCell(row, XDict::Mask, XUtility::lenToMask(szMask.toStdString()).c_str());
        AddCell(row, XDict::Gateway, "");
        AddCell(row, XDict::Metric, "");
        AddCell(row, XDict::Interface, "");
        m_Data.push_back(row);
    };

    QRegExp r1("(\\d+\\.\\d+\\.\\d+\\.\\d+)/(\\d+)");
    QRegExp r2("(\\d+\\.\\d+\\.\\d+\\.\\d+)\\s(\\d+\\.\\d+\\.\\d+\\.\\d+)\\s(\\d+)");

    typedef std::function<void(QString line)> ProcesserType;
    ProcesserType ProcLineT1 = [&r1, &AddRow](QString line){
        if ( -1 != r1.indexIn(line) )
            AddRow(r1.cap(1), r1.cap(2));
    };

    typedef unsigned long ulong;
    ProcesserType ProcLineT2 = [&r2, &AddRow](QString line){
        if ( -1 == r2.indexIn(line) )
            return;
        
        ulong s = ntohl(inet_addr(r2.cap(1).toStdString().c_str()));
        ulong e = ntohl(inet_addr(r2.cap(2).toStdString().c_str()));
        ulong re = ~e;

        int n = 0;
        ulong p = 1;
        while ((s&p) == (re&p))
        {
            p <<= 1;
            n++;
        }

        s >>= n;
        e >>= n;
        for (ulong i = s; i <= e; i++)
        {
            AddRow(XUtility::net_ntostr(htonl(i << n)).c_str(), XUtility::Vtostr(32 - n).c_str());
        }
    };

    ProcesserType TypeErr = [](QString line){};
    auto DiscernType = [&r1, &r2, &ProcLineT1, &ProcLineT2, &TypeErr](QString line){
        if ( -1 != r1.indexIn(line))
            return ProcLineT1;
        else if ( -1 != r2.indexIn(line))
            return ProcLineT2;
        else
            return TypeErr;
    };

    QFile f(szFile);
    if ( !f.open(QIODevice::ReadOnly|QIODevice::Text) )
        return false;

    QString line = f.readLine();
    ProcesserType Processer = DiscernType(line);

    beginResetModel();
    Processer(line);
    while (!f.atEnd())
        Processer(f.readLine());
    endResetModel();
    f.close();
    return true;
}

void XRouteListModel::Clear()
{
    beginResetModel();
    m_Header.clear();
    m_Data.clear();
    endResetModel();
}


