#ifndef RADIOBUTTON_H
#define RADIOBUTTON_H

#include <QPushButton>

class XRadioButton : public QPushButton
{
    Q_OBJECT
public:
    XRadioButton(QWidget *parent=0);

signals:
    void selected();

public slots:
    void me_clicked();
    void deselection();

};

#endif // RADIOBUTTON_H
