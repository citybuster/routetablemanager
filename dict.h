#ifndef DICT_H
#define DICT_H

class XDict
{
public:
    // Common
    static const char * const Index;
    static const char * const Name;

    // Route Table Column Name
    static const char * const Destination;
    static const char * const Mask;
    static const char * const Policy;
    static const char * const Gateway;
    static const char * const Interface;
    static const char * const ForwardType;
    static const char * const ForwardProto;
    static const char * const ForwardAge;
    static const char * const NextHopAS;
    static const char * const Metric;
    static const char * const Metric2;
    static const char * const Metric3;
    static const char * const Metric4;
    static const char * const Metric5;

    // MIB_IPFORWARD_TYPE
    static const char * const MibIprouteTypeOther;
    static const char * const MibIprouteTypeInvalid;
    static const char * const MibIprouteTypeDirect;
    static const char * const MibIprouteTypeIndirect;

    // MIB_IPFORWARD_PROTO
    static const char * const RouteProtocolOther;
    static const char * const RouteProtocolLocal;
    static const char * const RouteProtocolNetMgmt;
    static const char * const RouteProtocolIcmp;
    static const char * const RouteProtocolEgp;
    static const char * const RouteProtocolGgp;
    static const char * const RouteProtocolHello;
    static const char * const RouteProtocolRip;
    static const char * const RouteProtocolIsIs;
    static const char * const RouteProtocolEsIs;
    static const char * const RouteProtocolCisco;
    static const char * const RouteProtocolBbn;
    static const char * const RouteProtocolOspf;
    static const char * const RouteProtocolBgp;
    static const char * const RouteProtocolIdpr;
    static const char * const RouteProtocolEigrp;
    static const char * const RouteProtocolDvmrp;
    static const char * const RouteProtocolRpl;
    static const char * const RouteProtocolDhcp;







};

#endif // DICT_H
