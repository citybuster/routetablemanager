#include "routetablemodel.h"

#include "nethelper.h"
#include "dict.h"
#include "utility.h"

#include <QTimer>
#include <QEventLoop>

#include <boost/thread.hpp>


XRouteTableModel::XRouteTableModel(RootIndexType type, QObject *parent) :
    QAbstractItemModel(parent),
    m_pHeader(nullptr),
    m_pData(nullptr),
    m_RootType(type),
    m_eBatchOpting(NONE)
{
    if ( RouteTableIndex == type)
    {
        connect(this, SIGNAL(UpdateRouteTable(SpDataPair*)), SLOT(RefurbishRouteTable(SpDataPair*)));
        boost::thread t(boost::bind(&XRouteTableModel::InitValue, this));
    }

    if (InterfaceIndex == m_RootType)
    {
        connect(this, SIGNAL(UpdateInterface(Row*)), SLOT(RefurbishInterface(Row*)));
        InitIntf();
    }

    if (RouteTableIndex == type/* || InterfaceIndex == type*/)
    {
       boost::thread t(boost::bind(&XRouteTableModel::MonitorChange, this));
    }
}

QModelIndex XRouteTableModel::index(int row, int column, const QModelIndex &/*parent*/) const
{
    return createIndex(row, column);
}

QModelIndex XRouteTableModel::parent(const QModelIndex &/*child*/) const
{
    return QModelIndex();
}

int XRouteTableModel::rowCount(const QModelIndex &/*parent*/) const
{
    NetHelper helper;
    if (m_RootType == ForwardTypeIndex)
        return helper.GetIdToStrMap(ForwardType).size();
    else if (m_RootType == ForwardProtoIndex)
        return helper.GetIdToStrMap(ForwardProto).size();
    else if (m_RootType == InterfaceIndex)
    {
        if (m_pInterface) return m_pInterface->size();
    }
    else
    {
        QMutexLocker l(const_cast<QMutex*>(&m_Mutex));
        if (m_pData) return m_pData->size();
    }

    return 0;
}

int XRouteTableModel::columnCount(const QModelIndex& parent) const
{
    if (parent != QModelIndex())
        return 0;

    switch (m_RootType)
    {
    case ForwardTypeIndex:
    case ForwardProtoIndex:
    case InterfaceIndex:
        return 1;
        break;
    case RouteTableIndex:
    {
        QMutexLocker l(const_cast<QMutex*>(&m_Mutex));
        if (m_pHeader) return m_pHeader->size();
    }
        break;
    }
    return 0;
}

QVariant XRouteTableModel::data(const QModelIndex &index, int role) const
{
    NetHelper helper;
    if ( m_RootType == RouteTableIndex )
    {
        QMutexLocker l(const_cast<QMutex*>(&m_Mutex));
        return ((Cell&)(*m_pData)[index.row()][index.column()])[role];
    }
    else if (m_RootType == ForwardTypeIndex)
    {
        if (Qt::DisplayRole == role)
            return QString(helper.GetIdToStrMap(ForwardType)[index.row()+1].c_str());
        else if (Qt::UserRole == role)
            return index.row() + 1;
    }
    else if (m_RootType == ForwardProtoIndex)
    {
        if (Qt::DisplayRole == role)
            return QString(helper.GetIdToStrMap(ForwardProto)[index.row() + 1].c_str());
        else if (Qt::UserRole == role)
            return index.row() + 1;
    }
    else if (m_RootType == InterfaceIndex)
    {
        if (m_pInterface)
            return (*m_pInterface)[index.row()][role];
    }

    return QVariant();
}

QVariant XRouteTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if ( Qt::Horizontal == orientation )
    {
        QMutexLocker l(const_cast<QMutex*>(&m_Mutex));
        if (m_pHeader) return ((Cell&)(*m_pHeader)[section])[role];
    }
    if (role == Qt::DisplayRole) return section + 1;
    return QVariant();
}

QModelIndex XRouteTableModel::GetItemIndex(Item &item)
{
    int rowCnt = rowCount();
    int colCnt = columnCount();

    std::map<QString, bool> Keys;
    Keys[XDict::Destination] = true;
    Keys[XDict::Mask] = true;
//    Keys[XDict::Gateway] = true;
    Keys[XDict::Interface] = true;
    Keys[XDict::Metric] = true;
    Keys[XDict::ForwardType] = true;
    Keys[XDict::ForwardProto] = true;

    int Count = Keys.size();

    std::map<QString, bool> UserRoleKey;
    UserRoleKey[XDict::Interface] = true;
    UserRoleKey[XDict::ForwardType] = true;
    UserRoleKey[XDict::ForwardProto] = true;

    for (int r = 0; r < rowCnt; r++)
    {
        int Cnt = 0;
        for (int c = 0; c < colCnt; c++)
        {
            QString Key = headerData(c).toString();
            if (!Keys[Key]) continue;
            QString Val = data(index(r, c), UserRoleKey[Key] ? Qt::UserRole : Qt::DisplayRole).toString();
            if (Val == QString(item[Key.toStdString()].c_str()))
                Cnt++;
        }
        if (Cnt >= Count)
            return createIndex(r,0);
    }
    return QModelIndex();
}

void XRouteTableModel::RefurbishOpt(std::function<void ()> opt)
{
    m_eBatchOpting = StartImp;
    opt();
    m_eBatchOpting = EndImp;
}

void XRouteTableModel::InitValue()
{
    SpRow       pHeader(new Row);
    SpTable     pData(new Table);
    NetHelper   helper;
    IdTable     forwardType = helper.GetIdToStrMap(ForwardType);
    IdTable     forwardProto = helper.GetIdToStrMap(ForwardProto);
    IdTable     interfaceName = helper.GetIdToStrMap(InterfaceName);

    auto SetHeaderValue = [&pHeader](std::string ColName) {
        auto fResult = std::find_if(pHeader->begin(), pHeader->end(), [&ColName](Cell &cell)->bool{
            return cell[Qt::DisplayRole] == QVariant(QString(ColName.c_str()));
        });
        if (fResult == pHeader->end())
        {
            Cell item;
            item[Qt::DisplayRole] = QString(ColName.c_str());
            pHeader->push_back(item);
        }
    };

    auto AddCell = [&SetHeaderValue](Row &row, Item &item, std::string ColName, IdTable *pMap){
        SetHeaderValue(ColName);
        Cell cell;
        if (pMap)
        {
            unsigned int Id = XUtility::strtoV<unsigned int>(item[ColName]);
            cell[Qt::DisplayRole] = QString((*pMap)[Id].c_str());
            cell[Qt::UserRole] = Id;
        }
        else
        {
            cell[Qt::DisplayRole] = QString(item[ColName].c_str());
        }
        row.push_back(cell);
    };

    int range = 0, pos = 0;
    auto ProcVal = [this, &range, &pos, &pData, &AddCell, &forwardType, &forwardProto, &interfaceName](Item &item){
        Row row;
        AddCell(row, item, XDict::Destination, nullptr);
        AddCell(row, item, XDict::Mask, nullptr);
        AddCell(row, item, XDict::Gateway, nullptr);
        AddCell(row, item, XDict::Metric, nullptr);
        AddCell(row, item, XDict::Interface, &interfaceName);
        AddCell(row, item, XDict::ForwardAge, nullptr);
        AddCell(row, item, XDict::ForwardType, &forwardType);
        AddCell(row, item, XDict::ForwardProto, &forwardProto);

//         AddCell(row, item, XDict::Destination, nullptr);
//         AddCell(row, item, XDict::Mask, nullptr);
//         AddCell(row, item, XDict::Policy, nullptr);
//         AddCell(row, item, XDict::Gateway, nullptr);
//         AddCell(row, item, XDict::Interface, &interfaceName);
//         AddCell(row, item, XDict::ForwardType, &forwardType);
//         AddCell(row, item, XDict::ForwardProto, &forwardProto);
//         AddCell(row, item, XDict::ForwardAge, nullptr);
//         AddCell(row, item, XDict::NextHopAS, nullptr);
//         AddCell(row, item, XDict::Metric, nullptr);
//         AddCell(row, item, XDict::Metric2, nullptr);
//         AddCell(row, item, XDict::Metric3, nullptr);
//         AddCell(row, item, XDict::Metric4, nullptr);
//         AddCell(row, item, XDict::Metric5, nullptr);

        pData->push_back(row);
        emit LoadProgress(range, pos++);
    };

    Items items = helper.GetRouteTable([this, &range, &pos](int r, int p){
        range = r * 2;
        pos = p;
        emit LoadProgress(range, pos);
    });

    for_each(items.begin(), items.end(), ProcVal);

    emit UpdateRouteTable(new SpDataPair(pHeader, pData));
}

void XRouteTableModel::InitIntf()
{
    Row row;
    NetHelper helper;
    auto item = helper.GetIdToStrMap(InterfaceName);
    std::for_each(item.begin(), item.end(), [&row](Element ele){
        Cell cell;
        cell[Qt::DisplayRole] = ele.second.c_str();
        cell[Qt::UserRole] = XUtility::Vtostr(ele.first).c_str();
        row.push_back(cell);
    });
    emit UpdateInterface(new Row(row));
}

void XRouteTableModel::MonitorChange()
{
    auto WaitRouteChange = [](DWORD dwMilliseconds){
        HANDLE handle = NULL;
        OVERLAPPED overlap;
        overlap.hEvent = WSACreateEvent();
        NotifyRouteChange(&handle, &overlap);

        return WaitForSingleObject(overlap.hEvent, dwMilliseconds);
    };

    do
    {
        const int TIMEOUT = 300;
        DWORD dwRet = WaitRouteChange(TIMEOUT*2);
        if (m_eBatchOpting == StartImp)
            emit LoadProgress(0,0);
        if (dwRet == WAIT_TIMEOUT)
        {
            if (m_eBatchOpting != EndImp)
                continue;
            else if (WaitRouteChange(TIMEOUT) != WAIT_TIMEOUT)
                continue;
        }
        else if (dwRet == WAIT_OBJECT_0)
        {
            if (m_eBatchOpting == StartImp || m_eBatchOpting == EndImp)
                continue;
        }

        if (InterfaceName == m_RootType)
            InitIntf();

        if (RouteTableIndex == m_RootType)
            InitValue();

        m_eBatchOpting = NONE;
    } while (1);
}

void XRouteTableModel::RefurbishRouteTable(SpDataPair *routeTable)
{
    boost::shared_ptr<SpDataPair> spRouteTable(routeTable);
    beginResetModel();
    {
        QMutexLocker l(&m_Mutex);
        m_pHeader = spRouteTable->first;
        m_pData = spRouteTable->second;
    }
    endResetModel();
}

void XRouteTableModel::RefurbishInterface(Row *pRow)
{
    m_pInterface = boost::shared_ptr<Row>(pRow);
    emit dataChanged(createIndex(0, 0), createIndex(m_pInterface->size()-1, 0));
}

void XRouteTableModel::Refurbish()
{
    boost::thread t([this](){
        if (InterfaceName == m_RootType)
            InitIntf();

        if (RouteTableIndex == m_RootType)
            InitValue();
    });
}

