#-------------------------------------------------
#
# Project created by QtCreator 2014-07-14T11:45:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RouteTableManager
TEMPLATE = app

BOOST_ROOT = D:\boost\boost_1_55_0-msvc-12.0-32

INCLUDEPATH = $$BOOST_ROOT

CONFIG(debug, debug|release) {
LIBS += \
    "$$BOOST_ROOT\stage\lib\libboost_thread-vc120-mt-gd-1_55.lib" \
    "$$BOOST_ROOT\stage\lib\libboost_date_time-vc120-mt-gd-1_55.lib" \
    "$$BOOST_ROOT\stage\lib\libboost_system-vc120-mt-gd-1_55.lib" \
    "$$BOOST_ROOT\stage\lib\libboost_chrono-vc120-mt-gd-1_55.lib"
} else {
LIBS += \
    "$$BOOST_ROOT\stage\lib\libboost_thread-vc120-mt-1_55.lib" \
    "$$BOOST_ROOT\stage\lib\libboost_date_time-vc120-mt-1_55.lib" \
    "$$BOOST_ROOT\stage\lib\libboost_system-vc120-mt-1_55.lib" \
    "$$BOOST_ROOT\stage\lib\libboost_chrono-vc120-mt-1_55.lib"
}

DEFINES += _CRT_SECURE_NO_WARNINGS

SOURCES += main.cpp\
    nethelper.cpp \
    dialog.cpp \
    routetablemodel.cpp \
    dict.cpp \
    utility.cpp \
    utility.inl \
    radiobutton.cpp \
    routelistmodel.cpp

HEADERS  += \
    nethelper.h \
    dialog.h \
    routetablemodel.h \
    dict.h \
    utility.h \
    radiobutton.h \
    routelistmodel.h

FORMS    += \
    dialog.ui

RC_FILE += \
    RouteTableManager.rc

RESOURCES += \
    RouteTableManager.qrc


