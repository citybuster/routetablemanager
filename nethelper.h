#ifndef NETHELPER_H
#define NETHELPER_H

#include <WinSock2.h>
#include <Windows.h>

#include <IPHlpApi.h>
#pragma comment(lib, "Iphlpapi.lib")

#include <map>
#include <string>
#include <vector>
#include <functional>

#include <QString>

typedef std::map<std::string, std::string>      Item;
typedef std::vector<Item>                       Items;
typedef std::pair<unsigned int, std::string>    Element;
typedef std::map<unsigned int, std::string>     IdTable;

enum IdMapType { ForwardType, ForwardProto, InterfaceName };

class QAbstractItemModel;
class NetHelper
{
public:
    NetHelper();

public:
    Items GetInterfaces();
    Items GetRouteTable(std::function<void(int, int)> Progress = nullptr);
    std::vector<unsigned long> GetIfAddress(unsigned long IfIdx);
    bool AddIpforwardEntry(Item &item);
    bool ChangeIpforwardEntry(Item &item);
    bool DelIpforwardEntry(Item &item);
    IdTable GetIdToStrMap(IdMapType type);
    QString GetLastError() { return m_LastErrMsg; }
    int BatchImport(QAbstractItemModel *model);
    int BatchRemove(QAbstractItemModel *model);

    Item RetrieveBestRoute(QString ipAddr);

protected:
    void Backup(std::string filename);
    void Restore(std::string filename);
    void Clear();

protected:
    QString     m_LastErrMsg;
};

#endif // NETHELPER_H
