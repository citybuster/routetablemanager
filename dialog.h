#ifndef DIALOG_H
#define DIALOG_H

#include "ui_dialog.h"

#include <map>
#include <string>
#include <vector>
#include <functional>

typedef std::map<std::string, std::string>  Item;
typedef std::vector<Item>                   Items;

typedef std::function<void(Item &)>     OperatorFunc;

enum OPERATOR_TYPE { CtrlValueGatter, CtrlValueSetter };

enum PAGE_TYPE { PAGE_EDIT, PAGE_BATCH, PAGE_TEST };

class Dialog : public QDialog, public Ui::Dialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

protected:
    void Initialize();
    OperatorFunc GetOperator(OPERATOR_TYPE type);
    void ShowEditErrMsg(QString msg = "", bool bErr = true);
    void SelectPage(PAGE_TYPE t);
    void LocateRouteItem();

private slots:
    void on_pushButton_Refurbish_clicked();
    void on_tableView_RouteTable_clicked(const QModelIndex &index);
    void on_pushButton_Edit_Clear_clicked();
    void on_pushButton_Edit_Add_clicked();
    void on_pushButton_Edit_Save_clicked();
    void on_pushButton_Edit_Delete_clicked();
    void on_pushButton_Batch_Add_clicked();
    void on_pushButton_Route_Test_clicked();
    void on_pushButton_Batch_Browse_clicked();
    void on_pushButton_Batch_Load_clicked();
    void on_pushButton_Batch_Clear_clicked();
    void on_pushButton_Batch_Import_clicked();
    void on_pushButton_Batch_Remove_clicked();
    void on_lineEdit_Batch_Gateway_editingFinished();
    void on_lineEdit_Batch_Metric_editingFinished();
    void on_comboBox_Batch_Interface_currentIndexChanged(int index);
    void on_pushButton_Test_Test_clicked();
    void on_lineEdit_Test_Destination_returnPressed();
    
    void SetLoadProgress(int range, int pos);

};

#endif // DIALOG_H
