#ifndef ROUTELISTMODEL_H
#define ROUTELISTMODEL_H

#include <QAbstractItemModel>

class XRouteListModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit XRouteListModel(QObject *parent = 0);

    // QAbstractItemModel interface
public:
    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

public:
    void SetGateway(QString szGateway);
    void SetMetric(QString szMetric);
    void SetInterface(QString szIntf);
    bool Load(QString szFile);
    void Clear();

signals:

public slots:

private:
    typedef std::map<int, QVariant>     Cell;
    typedef std::vector<Cell>           Row;
    typedef std::vector<Row>            Table;
    typedef std::map<QString, Cell>     Param;
    enum { ColIndexRole = Qt::UserRole+1, DatIndexRole = Qt::UserRole+2 };

    Row     m_Header;
    Table   m_Data;
    Param   m_Param;
};

#endif // ROUTELISTMODEL_H
