#include "utility.h"

#include <WinSock2.h>

#include <string>

#include <boost/shared_array.hpp>


std::string XUtility::net_ntostr(unsigned long netaddr)
{
    in_addr in;
    in.S_un.S_addr = netaddr;
    return inet_ntoa(in);
}

std::string XUtility::wcs_tostdstr(wchar_t *pszName)
{
    int len = WideCharToMultiByte(CP_ACP, WC_COMPOSITECHECK, pszName, wcslen(pszName), NULL, NULL, NULL, NULL);
    boost::shared_array<char> buf(new char[len + 1]);
    memset(buf.get(), 0, len + 1);
    len = WideCharToMultiByte(CP_ACP, WC_COMPOSITECHECK, pszName, wcslen(pszName), buf.get(), len, NULL, NULL);
    return std::string(buf.get());
}

std::string XUtility::lenToMask(std::string l)
{
    auto len = strtoV<unsigned int>(l);
    unsigned long mask = 0;
    if (len) mask = 0xFFFFFFFF << (32 - len);
    in_addr in;
    in.S_un.S_addr = htonl(mask);
    return inet_ntoa(in);
}

