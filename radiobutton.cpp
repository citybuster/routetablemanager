#include "radiobutton.h"

XRadioButton::XRadioButton(QWidget *parent) :
    QPushButton(parent)
{
    connect(this, SIGNAL(clicked()), SLOT(me_clicked()));
}

void XRadioButton::me_clicked()
{
    if ( isChecked() )
        emit selected();
}

void XRadioButton::deselection()
{
    setChecked(false);
}
