#include "dialog.h"
#include "nethelper.h"

#include "routetablemodel.h"
#include "routelistmodel.h"
#include "dict.h"

#include <QFileDialog>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
    Initialize();
}

Dialog::~Dialog()
{
}

void Dialog::Initialize()
{
//    QRegExp IpExp("(1\d{2}|2(?:0|1|2|3|4)\d|25(?:0|1|2|3|4)|\d{2}|[1-9])\.(1\d{2}|2(?:0|1|2|3|4)\d|25(?:0|1|2|3|4|5)|\d{1,2})\.(1\d{2}|2(?:0|1|2|3|4)\d|25(?:0|1|2|3|4|5)|\d{1,2})\.(1\d{2}|2(?:0|1|2|3|4)\d|25(?:0|1|2|3|4)|\d{1,2})");
//    QRegExp MaskExp("(?:255|254|252|248|240|224|192|128)(?:\.0){3}|255\.(?:255|254|252|248|240|224|192|128)(?:\.0){2}|(?:255\.){2}(?:255|254|252|248|240|224|192|128)\.0|(?:255\.){3}(?:255|254|252|248|240|224|192|128)");

    {
        lineEdit_Edit_Dest->setInputMask("000.000.000.000");
        lineEdit_Edit_Mask->setInputMask("000.000.000.000");
        lineEdit_Edit_Gateway->setInputMask("000.000.000.000");

        comboBox_Edit_Interface->setModel(new XRouteTableModel(InterfaceIndex, this));
        comboBox_Edit_ForwardType->setModel(new XRouteTableModel(ForwardTypeIndex, this));
        comboBox_Edit_ForwardProto->setModel(new XRouteTableModel(ForwardProtoIndex, this));
    }
    {
        tableView_Batch_RouteList->setModel(new XRouteListModel(this));
        comboBox_Batch_Interface->setModel(new XRouteTableModel(InterfaceIndex, this));
    }

    {
        tableView_RouteTable->setModel(new XRouteTableModel(RouteTableIndex, this));
        tableView_RouteTable->resizeColumnsToContents();
    }

    {
        connect(tableView_RouteTable->model(), SIGNAL(LoadProgress(int,int)), SLOT(SetLoadProgress(int,int)));
        progressBar_LoadRouteTable->setVisible(false);
    }

    SelectPage(PAGE_EDIT);
}

void Dialog::LocateRouteItem()
{
    NetHelper helper;
    Item item = helper.RetrieveBestRoute(lineEdit_Test_Destination->text());

    XRouteTableModel *pMode = dynamic_cast<XRouteTableModel*>(tableView_RouteTable->model());
    if (!pMode) return;

    auto idx = pMode->GetItemIndex(item);

    tableView_RouteTable->selectRow(idx.row());
    tableView_RouteTable->setFocus();
    tableView_RouteTable->scrollTo(idx);
}


void Dialog::on_pushButton_Refurbish_clicked()
{
    XRouteTableModel *pMode1 = dynamic_cast<XRouteTableModel*>(tableView_RouteTable->model());
    pMode1->Refurbish();

    XRouteTableModel *pMode2 = dynamic_cast<XRouteTableModel*>(comboBox_Edit_Interface->model());
    pMode2->Refurbish();
}

OperatorFunc Dialog::GetOperator(OPERATOR_TYPE type)
{
    if ( CtrlValueGatter == type )
    {
        return [this](Item &item){
            item[XDict::Destination] = lineEdit_Edit_Dest->text().toStdString();
            item[XDict::Mask]        = lineEdit_Edit_Mask->text().toStdString();
            item[XDict::Gateway]     = lineEdit_Edit_Gateway->text().toStdString();
            item[XDict::Metric]      = lineEdit_Edit_Metric->text().toStdString();
            item[XDict::Interface]   = comboBox_Edit_Interface->currentData().toString().toStdString();
            item[XDict::ForwardAge]  = lineEdit_Edit_ForwardAge->text().toStdString();
            item[XDict::ForwardType] = comboBox_Edit_ForwardType->currentData().toString().toStdString();
            item[XDict::ForwardProto]= comboBox_Edit_ForwardProto->currentData().toString().toStdString();
        };
    }
    else if ( CtrlValueSetter == type )
    {
        return [this](Item &item){
            lineEdit_Edit_Dest->setText(item[XDict::Destination].c_str());
            lineEdit_Edit_Mask->setText(item[XDict::Mask].c_str());
            lineEdit_Edit_Gateway->setText(item[XDict::Gateway].c_str());
            lineEdit_Edit_Metric->setText(item[XDict::Metric].c_str());
            int idx = comboBox_Edit_Interface->findData(item[XDict::Interface].c_str());
            comboBox_Edit_Interface->setCurrentIndex(idx);
            lineEdit_Edit_ForwardAge->setText(item[XDict::ForwardAge].c_str());
            idx = comboBox_Edit_ForwardType->findData(item[XDict::ForwardType].c_str());
            comboBox_Edit_ForwardType->setCurrentIndex(idx);
            idx = comboBox_Edit_ForwardProto->findData(item[XDict::ForwardProto].c_str());
            comboBox_Edit_ForwardProto->setCurrentIndex(idx);
        };
    }

    return [](Item &){};
}

void Dialog::ShowEditErrMsg(QString msg, bool bErr)
{
    switch (stackedWidget->currentIndex())
    {
    case 0:
        label_Edit_ErrMsg->setText(msg);
        if (bErr) label_Edit_ErrMsg->setStyleSheet("color: rgb(255, 0, 0);");
        else label_Edit_ErrMsg->setStyleSheet("color: rgb(0, 255, 0);");
        break;
    case 1:
        label_Batch_ErrMsg->setText(msg);
        if (bErr) label_Batch_ErrMsg->setStyleSheet("color: rgb(255, 0, 0);");
        else label_Batch_ErrMsg->setStyleSheet("color: rgb(0, 255, 0);");
        break;
    case 2:
        label_Test_ErrMsg->setText(msg);
        if (bErr) label_Test_ErrMsg->setStyleSheet("color: rgb(255, 0, 0);");
        else label_Test_ErrMsg->setStyleSheet("color: rgb(0, 255, 0);");
        break;
    }
}

void Dialog::SelectPage(PAGE_TYPE t)
{
    widget_Edit->setVisible(false);
    widget_Batch->setVisible(false);
    widget_Test->setVisible(false);
    switch(t)
    {
    case PAGE_EDIT:  widget_Edit->setVisible(true); break;
    case PAGE_BATCH: widget_Batch->setVisible(true); break;
    case PAGE_TEST:  widget_Test->setVisible(true); break;
    }
    stackedWidget->setCurrentIndex(t);
}

void Dialog::on_tableView_RouteTable_clicked(const QModelIndex &index)
{
    auto GetRouteTableValue = [&index]() -> Item{
        const XRouteTableModel *pMode = dynamic_cast<const XRouteTableModel*>(index.model());
        Item item;

        int colCnt = pMode->columnCount(index.parent());
        for (int i = 0; i < colCnt; i++)
        {
            std::string key = pMode->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString().toStdString();
            std::string value = pMode->data(pMode->index(index.row(), i, index.parent()), 
                (XDict::Interface == key || XDict::ForwardType == key || XDict::ForwardProto == key) ? Qt::UserRole : Qt::DisplayRole).toString().toStdString();
            item[key] = value;
        }
        return item;
    };

    GetOperator(CtrlValueSetter)(GetRouteTableValue());
    ShowEditErrMsg();
    SelectPage(PAGE_EDIT);
    pushButton_Batch_Add->setChecked(false);
    pushButton_Route_Test->setChecked(false);
}

void Dialog::on_pushButton_Edit_Clear_clicked()
{
    GetOperator(CtrlValueSetter)(Item());
}

void Dialog::on_pushButton_Edit_Add_clicked()
{
    Item item;
    GetOperator(CtrlValueGatter)(item);

    NetHelper helper;
    if ( helper.AddIpforwardEntry(item) )
        ShowEditErrMsg("Success", false);
    else
        ShowEditErrMsg(helper.GetLastError());
}

void Dialog::on_pushButton_Edit_Save_clicked()
{
    Item item;
    GetOperator(CtrlValueGatter)(item);

    NetHelper helper;
    if ( helper.ChangeIpforwardEntry(item) )
        ShowEditErrMsg("Success", false);
    else
        ShowEditErrMsg(helper.GetLastError());
}

void Dialog::on_pushButton_Edit_Delete_clicked()
{
    Item item;
    GetOperator(CtrlValueGatter)(item);

    NetHelper helper;
    if (helper.DelIpforwardEntry(item))
        ShowEditErrMsg("Success", false);
    else
        ShowEditErrMsg(helper.GetLastError());
}

void Dialog::on_pushButton_Batch_Add_clicked()
{
    SelectPage(pushButton_Batch_Add->isChecked()?PAGE_BATCH:PAGE_EDIT);
}

void Dialog::on_pushButton_Route_Test_clicked()
{
    SelectPage(pushButton_Route_Test->isChecked()?PAGE_TEST:PAGE_EDIT);
}

void Dialog::on_pushButton_Batch_Browse_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Route File"), QString(), tr("Text file(*.txt);;All file(*.*)"));
    if (fileName.isEmpty()) return ;

    lineEdit_Batch_File->setText(fileName);
}

void Dialog::on_pushButton_Batch_Load_clicked()
{
    XRouteListModel *pModel = dynamic_cast<XRouteListModel*>(tableView_Batch_RouteList->model());
    if (!pModel) return ;

    pModel->Load(lineEdit_Batch_File->text());
    tableView_Batch_RouteList->resizeColumnsToContents();
}

void Dialog::on_pushButton_Batch_Clear_clicked()
{
    XRouteListModel *pModel = dynamic_cast<XRouteListModel*>(tableView_Batch_RouteList->model());
    if (!pModel) return ;

    pModel->Clear();
}

void Dialog::on_pushButton_Batch_Import_clicked()
{
    XRouteListModel *pModel = dynamic_cast<XRouteListModel*>(tableView_Batch_RouteList->model());
    if (!pModel) return ;

    XRouteTableModel *pMode = dynamic_cast<XRouteTableModel*>(tableView_RouteTable->model());
    if (!pMode) return;

    ShowEditErrMsg();
    pMode->RefurbishOpt([this, &pModel](){
        NetHelper helper;
        int cnt = helper.BatchImport(pModel);
        if (cnt)
            ShowEditErrMsg(helper.GetLastError());
        else
            ShowEditErrMsg("Success", false);
    });
}

void Dialog::on_pushButton_Batch_Remove_clicked()
{
    XRouteListModel *pModel = dynamic_cast<XRouteListModel*>(tableView_Batch_RouteList->model());
    if (!pModel) return ;

    XRouteTableModel *pMode = dynamic_cast<XRouteTableModel*>(tableView_RouteTable->model());
    if (!pMode) return;

    ShowEditErrMsg();
    pMode->RefurbishOpt([this, &pModel](){
        NetHelper helper;
        int cnt = helper.BatchRemove(pModel);
        if (cnt)
            ShowEditErrMsg(helper.GetLastError());
        else
            ShowEditErrMsg("Success", false);
    });
}

void Dialog::on_lineEdit_Batch_Gateway_editingFinished()
{
    XRouteListModel *pModel = dynamic_cast<XRouteListModel*>(tableView_Batch_RouteList->model());
    if (!pModel) return ;

    pModel->SetGateway(lineEdit_Batch_Gateway->text());
    tableView_Batch_RouteList->resizeColumnsToContents();
}

void Dialog::on_lineEdit_Batch_Metric_editingFinished()
{
    XRouteListModel *pModel = dynamic_cast<XRouteListModel*>(tableView_Batch_RouteList->model());
    if (!pModel) return ;

    pModel->SetMetric(lineEdit_Batch_Metric->text());
    tableView_Batch_RouteList->resizeColumnsToContents();
}

void Dialog::on_comboBox_Batch_Interface_currentIndexChanged(int index)
{
    XRouteListModel *pModel = dynamic_cast<XRouteListModel*>(tableView_Batch_RouteList->model());
    if (!pModel) return ;

    pModel->SetInterface(comboBox_Batch_Interface->itemData(index).toString());
    tableView_Batch_RouteList->resizeColumnsToContents();
}


void Dialog::on_pushButton_Test_Test_clicked()
{
    LocateRouteItem();
}

void Dialog::on_lineEdit_Test_Destination_returnPressed()
{
    LocateRouteItem();
}

void Dialog::SetLoadProgress(int range, int pos)
{
    if (pos == 0)
    {
        progressBar_LoadRouteTable->setRange(0, range);
        progressBar_LoadRouteTable->setValue(pos);
        progressBar_LoadRouteTable->setVisible(true);
    }
    else if ( pos == range)
    {
        progressBar_LoadRouteTable->setVisible(false);
    }
    else
        progressBar_LoadRouteTable->setValue(pos);
}

