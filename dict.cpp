#include "dict.h"

// Common
const char * const XDict::Index = "Index";
const char * const XDict::Name  = "Name";

// Route Table Column Name
const char * const XDict::Destination   = "Distination";
const char * const XDict::Mask          = "Mask";
const char * const XDict::Policy        = "Policy";
const char * const XDict::Gateway       = "Gateway";
const char * const XDict::Interface     = "Interface";
const char * const XDict::ForwardType   = "ForwardType";
const char * const XDict::ForwardProto  = "ForwardProto";
const char * const XDict::ForwardAge    = "ForwardAge";
const char * const XDict::NextHopAS     = "NextHopAS";
const char * const XDict::Metric        = "Metric";
const char * const XDict::Metric2       = "Metric2";
const char * const XDict::Metric3       = "Metric3";
const char * const XDict::Metric4       = "Metric4";
const char * const XDict::Metric5       = "Metric5";

// MIB_IPFORWARD_TYPE
const char * const XDict::MibIprouteTypeOther   = "MIB_IPROUTE_TYPE_OTHER";
const char * const XDict::MibIprouteTypeInvalid = "MIB_IPROUTE_TYPE_INVALID";
const char * const XDict::MibIprouteTypeDirect  = "MIB_IPROUTE_TYPE_DIRECT";
const char * const XDict::MibIprouteTypeIndirect= "MIB_IPROUTE_TYPE_INDIRECT";

// MIB_IPFORWARD_PROTO
const char * const XDict::RouteProtocolOther    = "RouteProtocolOther";
const char * const XDict::RouteProtocolLocal    = "RouteProtocolLocal";
const char * const XDict::RouteProtocolNetMgmt  = "RouteProtocolNetMgmt";
const char * const XDict::RouteProtocolIcmp     = "RouteProtocolIcmp";
const char * const XDict::RouteProtocolEgp      = "RouteProtocolEgp";
const char * const XDict::RouteProtocolGgp      = "RouteProtocolGgp";
const char * const XDict::RouteProtocolHello    = "RouteProtocolHello";
const char * const XDict::RouteProtocolRip      = "RouteProtocolRip";
const char * const XDict::RouteProtocolIsIs     = "RouteProtocolIsIs";
const char * const XDict::RouteProtocolEsIs     = "RouteProtocolEsIs";
const char * const XDict::RouteProtocolCisco    = "RouteProtocolCisco";
const char * const XDict::RouteProtocolBbn      = "RouteProtocolBbn";
const char * const XDict::RouteProtocolOspf     = "RouteProtocolOspf";
const char * const XDict::RouteProtocolBgp      = "RouteProtocolBgp";
const char * const XDict::RouteProtocolIdpr     = "RouteProtocolIdpr";
const char * const XDict::RouteProtocolEigrp    = "RouteProtocolEigrp";
const char * const XDict::RouteProtocolDvmrp    = "RouteProtocolDvmrp";
const char * const XDict::RouteProtocolRpl      = "RouteProtocolRpl";
const char * const XDict::RouteProtocolDhcp     = "RouteProtocolDhcp";

