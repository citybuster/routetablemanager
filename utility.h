#ifndef UTILITY_H
#define UTILITY_H

#include <string>

class XUtility
{
public:
    static std::string net_ntostr(unsigned long netaddr);
    static std::string wcs_tostdstr(wchar_t *pszName);
    template <class T> static T strtoV(std::string str);
    template <class T> static std::string Vtostr(T l);
    template <class T> static T strtoEnum(std::string str);
    static std::string lenToMask(std::string l);



};

#include "utility.inl"

#endif // UTILITY_H
