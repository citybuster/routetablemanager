#include "nethelper.h"

#include "dict.h"
#include "utility.h"

#include <QFile>
#include <QDataStream>
#include <QTextCodec>
#include <QAbstractItemModel>

#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>

NetHelper::NetHelper()
{
}

Items NetHelper::GetInterfaces()
{
    ULONG SizePointer = 0;
    boost::shared_ptr<IP_ADAPTER_ADDRESSES> pIpAdapterAddresses;
    if (ERROR_BUFFER_OVERFLOW != GetAdaptersAddresses(AF_INET, GAA_FLAG_INCLUDE_ALL_COMPARTMENTS, 0, pIpAdapterAddresses.get(), &SizePointer))
    {
        throw std::runtime_error("GetAdaptersAddresses return value error");
    }

    pIpAdapterAddresses = boost::shared_ptr<IP_ADAPTER_ADDRESSES>((PIP_ADAPTER_ADDRESSES)malloc(SizePointer), free);
    memset(pIpAdapterAddresses.get(), 0, SizePointer);

    if (ERROR_SUCCESS != GetAdaptersAddresses(AF_INET, GAA_FLAG_INCLUDE_ALL_COMPARTMENTS, 0, pIpAdapterAddresses.get(), &SizePointer))
    {
        throw std::runtime_error("GetAdaptersAddresses return value error");
    }

    Items items;
    PIP_ADAPTER_ADDRESSES pNext = pIpAdapterAddresses.get();
    while (pNext)
    {
        QTextCodec *codec = QTextCodec::codecForName("GB18030");
        Item item;
        item[XDict::Index]  = XUtility::Vtostr<unsigned long>(pNext->IfIndex);
        item[XDict::Name]   = codec->toUnicode(XUtility::wcs_tostdstr(pNext->Description).c_str()).toStdString();
        items.push_back(item);
        pNext = pNext->Next;
    }

    return items;
}

Items NetHelper::GetRouteTable(std::function<void(int, int)> Progress)
{
    auto SaveForwardRow = [](Item &item, PMIB_IPFORWARDROW pIpForwardRow) {
        item[XDict::Destination]    = XUtility::net_ntostr(pIpForwardRow->dwForwardDest);
        item[XDict::Mask]           = XUtility::net_ntostr(pIpForwardRow->dwForwardMask);
        item[XDict::Policy]         = XUtility::Vtostr(pIpForwardRow->dwForwardPolicy);
        item[XDict::Gateway]        = XUtility::net_ntostr(pIpForwardRow->dwForwardNextHop);
        item[XDict::Interface]      = XUtility::Vtostr(pIpForwardRow->dwForwardIfIndex);
        item[XDict::ForwardType]    = XUtility::Vtostr(pIpForwardRow->ForwardType);
        item[XDict::ForwardProto]   = XUtility::Vtostr(pIpForwardRow->ForwardProto);
        item[XDict::ForwardAge]     = XUtility::Vtostr(pIpForwardRow->dwForwardAge);
        item[XDict::NextHopAS]      = XUtility::Vtostr(pIpForwardRow->dwForwardNextHopAS);
        item[XDict::Metric]         = XUtility::Vtostr(pIpForwardRow->dwForwardMetric1);
        item[XDict::Metric2]        = XUtility::Vtostr(pIpForwardRow->dwForwardMetric2);
        item[XDict::Metric3]        = XUtility::Vtostr(pIpForwardRow->dwForwardMetric3);
        item[XDict::Metric4]        = XUtility::Vtostr(pIpForwardRow->dwForwardMetric4);
        item[XDict::Metric5]        = XUtility::Vtostr(pIpForwardRow->dwForwardMetric5);
    };

    ULONG dwSize = 0;
    PMIB_IPFORWARDTABLE pIpForwardTable = NULL;
    GetIpForwardTable(pIpForwardTable, &dwSize, 0);
    pIpForwardTable = (PMIB_IPFORWARDTABLE)malloc(dwSize);
    memset(pIpForwardTable, 0, dwSize);

    Items ret;
    GetIpForwardTable(pIpForwardTable, &dwSize, FALSE);
    for (unsigned int i = 0; i < pIpForwardTable->dwNumEntries; i++)
    {
        Item item;
        SaveForwardRow(item, &pIpForwardTable->table[i]);
        ret.push_back(item);
        if (Progress) Progress(pIpForwardTable->dwNumEntries - 1, i);
    }

    return ret;
}

std::vector<unsigned long> NetHelper::GetIfAddress(unsigned long IfIdx)
{
    ULONG SizePointer = 0;
    boost::shared_ptr<IP_ADAPTER_ADDRESSES> pIpAdapterAddresses;
    if (ERROR_BUFFER_OVERFLOW != GetAdaptersAddresses(AF_INET, GAA_FLAG_INCLUDE_ALL_COMPARTMENTS, 0, pIpAdapterAddresses.get(), &SizePointer))
    {
        throw std::runtime_error("GetAdaptersAddresses return value error");
    }

    pIpAdapterAddresses = boost::shared_ptr<IP_ADAPTER_ADDRESSES>((PIP_ADAPTER_ADDRESSES)malloc(SizePointer), free);
    memset(pIpAdapterAddresses.get(), 0, SizePointer);

    if (ERROR_SUCCESS != GetAdaptersAddresses(AF_INET, GAA_FLAG_INCLUDE_ALL_COMPARTMENTS, 0, pIpAdapterAddresses.get(), &SizePointer))
    {
        throw std::runtime_error("GetAdaptersAddresses return value error");
    }

    std::vector<unsigned long> ret;
    PIP_ADAPTER_ADDRESSES pNext = pIpAdapterAddresses.get();
    while (pNext)
    {
        if (pNext->IfIndex != IfIdx)
        {
            pNext = pNext->Next;
            continue;
        }

        PIP_ADAPTER_UNICAST_ADDRESS pN = pNext->FirstUnicastAddress;
        while (pN)
        {
            ret.push_back(((sockaddr_in*)pN->Address.lpSockaddr)->sin_addr.s_addr);
            pN = pN->Next;
        }
        break;
    }
    return ret;
}

bool NetHelper::AddIpforwardEntry(Item &item)
{
    MIB_IPFORWARDROW IpForwardRow;
    memset(&IpForwardRow, 0, sizeof(IpForwardRow));

    IpForwardRow.dwForwardDest      = inet_addr(item[XDict::Destination].c_str());
    IpForwardRow.dwForwardMask      = inet_addr(item[XDict::Mask].c_str());
    //IpForwardRow.dwForwardPolicy    = XUtility::strtoV<unsigned long>(item[XDict::Policy]);
    IpForwardRow.dwForwardNextHop   = inet_addr(item[XDict::Gateway].c_str());
    IpForwardRow.dwForwardIfIndex   = XUtility::strtoV<unsigned long>(item[XDict::Interface]);
    IpForwardRow.ForwardType        = XUtility::strtoEnum<MIB_IPFORWARD_TYPE>(item[XDict::ForwardType]);
    IpForwardRow.ForwardProto       = XUtility::strtoEnum<MIB_IPFORWARD_PROTO>(item[XDict::ForwardProto]);
    //IpForwardRow.dwForwardAge
    //IpForwardRow.dwForwardNextHopAS
    IpForwardRow.dwForwardMetric1   = XUtility::strtoV<unsigned long>(item[XDict::Metric]);
    //IpForwardRow.dwForwardMetric2
    //IpForwardRow.dwForwardMetric3
    //IpForwardRow.dwForwardMetric4
    //IpForwardRow.dwForwardMetric5

    DWORD dwRet = CreateIpForwardEntry(&IpForwardRow);
    switch (dwRet)
    {
    case ERROR_ACCESS_DENIED: m_LastErrMsg = "Access is denied."; break;
    case ERROR_INVALID_PARAMETER: m_LastErrMsg = "An input parameter is invalid, no action was taken."; break;
    case ERROR_NOT_SUPPORTED: m_LastErrMsg = "The IPv4 transport is not configured on the local computer."; break;
    case NO_ERROR:  return true;
    default:
    {
        LPTSTR lpMsgBuf;
        FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL, dwRet, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
        m_LastErrMsg = QTextCodec::codecForName("GB18030")->toUnicode(XUtility::wcs_tostdstr(lpMsgBuf).c_str());
        LocalFree(lpMsgBuf);
    }
    }

    return false;
}

bool NetHelper::ChangeIpforwardEntry(Item &item)
{
    MIB_IPFORWARDROW IpForwardRow;
    memset(&IpForwardRow, 0, sizeof(IpForwardRow));

    IpForwardRow.dwForwardDest      = inet_addr(item[XDict::Destination].c_str());
    IpForwardRow.dwForwardMask      = inet_addr(item[XDict::Mask].c_str());
    //IpForwardRow.dwForwardPolicy    = XUtility::strtoV<unsigned long>(item[XDict::Policy]);
    IpForwardRow.dwForwardNextHop   = inet_addr(item[XDict::Gateway].c_str());
    IpForwardRow.dwForwardIfIndex   = XUtility::strtoV<unsigned long>(item[XDict::Interface]);
    IpForwardRow.ForwardType        = XUtility::strtoEnum<MIB_IPFORWARD_TYPE>(item[XDict::ForwardType]);
    IpForwardRow.ForwardProto       = XUtility::strtoEnum<MIB_IPFORWARD_PROTO>(item[XDict::ForwardProto]);
    //IpForwardRow.dwForwardAge
    //IpForwardRow.dwForwardNextHopAS
    IpForwardRow.dwForwardMetric1   = XUtility::strtoV<unsigned long>(item[XDict::Metric]);
    //IpForwardRow.dwForwardMetric2
    //IpForwardRow.dwForwardMetric3
    //IpForwardRow.dwForwardMetric4
    //IpForwardRow.dwForwardMetric5

    DWORD dwRet = SetIpForwardEntry(&IpForwardRow);
    switch (dwRet)
    {
    case ERROR_ACCESS_DENIED: m_LastErrMsg = "Access is denied."; break;
    case ERROR_INVALID_PARAMETER: m_LastErrMsg = "An input parameter is invalid, no action was taken."; break;
    case ERROR_NOT_SUPPORTED: m_LastErrMsg = "The IPv4 transport is not configured on the local computer."; break;
    case NO_ERROR:  return true;
    default:
    {
        LPTSTR lpMsgBuf;
        FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL, dwRet, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
        m_LastErrMsg = QTextCodec::codecForName("GB18030")->toUnicode(XUtility::wcs_tostdstr(lpMsgBuf).c_str());
        LocalFree(lpMsgBuf);
    }
    }

    return false;
}

bool NetHelper::DelIpforwardEntry(Item &item)
{
    MIB_IPFORWARDROW IpForwardRow;
    memset(&IpForwardRow, 0, sizeof(IpForwardRow));

    IpForwardRow.dwForwardDest      = inet_addr(item[XDict::Destination].c_str());
    IpForwardRow.dwForwardMask      = inet_addr(item[XDict::Mask].c_str());
    //IpForwardRow.dwForwardPolicy    = XUtility::strtoV<unsigned long>(item[XDict::Policy]);
    IpForwardRow.dwForwardNextHop   = inet_addr(item[XDict::Gateway].c_str());
    IpForwardRow.dwForwardIfIndex   = XUtility::strtoV<unsigned long>(item[XDict::Interface]);
    IpForwardRow.ForwardType        = XUtility::strtoEnum<MIB_IPFORWARD_TYPE>(item[XDict::ForwardType]);
    IpForwardRow.ForwardProto       = XUtility::strtoEnum<MIB_IPFORWARD_PROTO>(item[XDict::ForwardProto]);
    //IpForwardRow.dwForwardAge
    //IpForwardRow.dwForwardNextHopAS
    IpForwardRow.dwForwardMetric1   = XUtility::strtoV<unsigned long>(item[XDict::Metric]);
    //IpForwardRow.dwForwardMetric2
    //IpForwardRow.dwForwardMetric3
    //IpForwardRow.dwForwardMetric4
    //IpForwardRow.dwForwardMetric5

    DWORD dwRet = DeleteIpForwardEntry(&IpForwardRow);

    switch (dwRet)
    {
    case ERROR_ACCESS_DENIED: m_LastErrMsg = "Access is denied."; break;
    case ERROR_INVALID_PARAMETER: m_LastErrMsg = "An input parameter is invalid, no action was taken."; break;
    case ERROR_NOT_FOUND: m_LastErrMsg = "The pRoute parameter points to a route entry that does not exist."; break;
    case ERROR_NOT_SUPPORTED: m_LastErrMsg = "The IPv4 transport is not configured on the local computer.";
    case NO_ERROR:
    default:
        return true;
    }

    return false;
}

IdTable NetHelper::GetIdToStrMap(IdMapType type)
{
    if ( ForwardType == type )
    {
        IdTable forwardType;
        {
            forwardType[MIB_IPROUTE_TYPE_OTHER]     = XDict::MibIprouteTypeOther;
            forwardType[MIB_IPROUTE_TYPE_INVALID]   = XDict::MibIprouteTypeInvalid;
            forwardType[MIB_IPROUTE_TYPE_DIRECT]    = XDict::MibIprouteTypeDirect;
            forwardType[MIB_IPROUTE_TYPE_INDIRECT]  = XDict::MibIprouteTypeIndirect;
        }
        return forwardType;
    }
    else if ( ForwardProto == type )
    {
        IdTable forwardProto;
        {
            forwardProto[RouteProtocolOther]    = XDict::RouteProtocolOther;
            forwardProto[RouteProtocolLocal]    = XDict::RouteProtocolLocal;
            forwardProto[RouteProtocolNetMgmt]  = XDict::RouteProtocolNetMgmt;
            forwardProto[RouteProtocolIcmp]     = XDict::RouteProtocolIcmp;
            forwardProto[RouteProtocolEgp]      = XDict::RouteProtocolEgp;
            forwardProto[RouteProtocolGgp]      = XDict::RouteProtocolGgp;
            forwardProto[RouteProtocolHello]    = XDict::RouteProtocolHello;
            forwardProto[RouteProtocolRip]      = XDict::RouteProtocolRip;
            forwardProto[RouteProtocolIsIs]     = XDict::RouteProtocolIsIs;
            forwardProto[RouteProtocolEsIs]     = XDict::RouteProtocolEsIs;
            forwardProto[RouteProtocolCisco]    = XDict::RouteProtocolCisco;
            forwardProto[RouteProtocolBbn]      = XDict::RouteProtocolBbn;
            forwardProto[RouteProtocolOspf]     = XDict::RouteProtocolOspf;
            forwardProto[RouteProtocolBgp]      = XDict::RouteProtocolBgp;
            forwardProto[RouteProtocolIdpr]     = XDict::RouteProtocolIdpr;
            forwardProto[RouteProtocolEigrp]    = XDict::RouteProtocolEigrp;
            forwardProto[RouteProtocolDvmrp]    = XDict::RouteProtocolDvmrp;
            forwardProto[RouteProtocolRpl]      = XDict::RouteProtocolRpl;
            forwardProto[RouteProtocolDhcp]     = XDict::RouteProtocolDhcp;
        }
        return forwardProto;
    }
    else if ( InterfaceName == type )
    {
        IdTable InterfaceList;
        Items items = GetInterfaces();
        for_each(items.begin(), items.end(), [&InterfaceList](Item &item){
            InterfaceList[XUtility::strtoV<unsigned int>(item[XDict::Index])] = item[XDict::Name];
        });
        return InterfaceList;
    }

    return IdTable();
}

int NetHelper::BatchImport(QAbstractItemModel *model)
{
    int rowCnt = model->rowCount();
    int colCnt = model->columnCount();
    for (int r = 0; r < rowCnt; r++)
    {
        Item item;
        for (int c = 0; c < colCnt; c++)
        {
            std::string key = model->headerData(c, Qt::Horizontal).toString().toStdString();
            std::string Val = model->data(model->index(r, c), key == XDict::Interface?Qt::UserRole:Qt::DisplayRole).toString().toStdString();
            item[key] = Val;
        }
        item[XDict::ForwardType] = "4";     // MIB_IPROUTE_TYPE_INDIRECT
        item[XDict::ForwardProto] = "3";    // RouteProtocolNetMgmt

        if ( !AddIpforwardEntry(item) )
        {
            return rowCnt - r - 1;
        }
    }

    return 0;
}

int NetHelper::BatchRemove(QAbstractItemModel *model)
{
    int rowCnt = model->rowCount();
    int colCnt = model->columnCount();
    for (int r = 0; r < rowCnt; r++)
    {
        Item item;
        for (int c = 0; c < colCnt; c++)
        {
            std::string key = model->headerData(c, Qt::Horizontal).toString().toStdString();
            std::string Val = model->data(model->index(r, c), key == XDict::Interface?Qt::UserRole:Qt::DisplayRole).toString().toStdString();
            item[key] = Val;
        }
        item[XDict::ForwardType] = "4";     // MIB_IPROUTE_TYPE_INDIRECT
        item[XDict::ForwardProto] = "3";    // RouteProtocolNetMgmt

        if ( !DelIpforwardEntry(item) )
        {
            return rowCnt - r - 1;
        }
    }

    return 0;
}

Item NetHelper::RetrieveBestRoute(QString ipAddr)
{
    auto SaveForwardRow = [](Item &item, PMIB_IPFORWARDROW pIpForwardRow) {
        item[XDict::Destination]    = XUtility::net_ntostr(pIpForwardRow->dwForwardDest);
        item[XDict::Mask]           = XUtility::net_ntostr(pIpForwardRow->dwForwardMask);
        item[XDict::Policy]         = XUtility::Vtostr(pIpForwardRow->dwForwardPolicy);
        item[XDict::Gateway]        = XUtility::net_ntostr(pIpForwardRow->dwForwardNextHop);
        item[XDict::Interface]      = XUtility::Vtostr(pIpForwardRow->dwForwardIfIndex);
        item[XDict::ForwardType]    = XUtility::Vtostr(pIpForwardRow->ForwardType);
        item[XDict::ForwardProto]   = XUtility::Vtostr(pIpForwardRow->ForwardProto);
        item[XDict::ForwardAge]     = XUtility::Vtostr(pIpForwardRow->dwForwardAge);
        item[XDict::NextHopAS]      = XUtility::Vtostr(pIpForwardRow->dwForwardNextHopAS);
        item[XDict::Metric]         = XUtility::Vtostr(pIpForwardRow->dwForwardMetric1);
        item[XDict::Metric2]        = XUtility::Vtostr(pIpForwardRow->dwForwardMetric2);
        item[XDict::Metric3]        = XUtility::Vtostr(pIpForwardRow->dwForwardMetric3);
        item[XDict::Metric4]        = XUtility::Vtostr(pIpForwardRow->dwForwardMetric4);
        item[XDict::Metric5]        = XUtility::Vtostr(pIpForwardRow->dwForwardMetric5);
    };

    MIB_IPFORWARDROW ipForward;
    memset(&ipForward, 0, sizeof(ipForward));
    GetBestRoute(inet_addr(ipAddr.toLocal8Bit().constData()), 0, &ipForward);
    Item item;
    SaveForwardRow(item, &ipForward);
    return item;
}

void NetHelper::Backup(std::string filename)
{
    ULONG dwSize = 0;
    PMIB_IPFORWARDTABLE pIpForwardTable = NULL;
    GetIpForwardTable(pIpForwardTable, &dwSize, 0);
    pIpForwardTable = (PMIB_IPFORWARDTABLE)malloc(dwSize);
    memset(pIpForwardTable, 0, dwSize);

    QFile file(filename.c_str());
    file.open(QIODevice::WriteOnly);

    QDataStream stream(&file);

    GetIpForwardTable(pIpForwardTable, &dwSize, FALSE);
    stream << (quint32)pIpForwardTable->dwNumEntries;
    for (unsigned int i = 0; i < pIpForwardTable->dwNumEntries; i++)
    {
        stream << (quint32)pIpForwardTable->table[i].dwForwardDest;
        stream << (quint32)pIpForwardTable->table[i].dwForwardMask;
        stream << (quint32)pIpForwardTable->table[i].dwForwardPolicy;
        stream << (quint32)pIpForwardTable->table[i].dwForwardNextHop;
        stream << (quint32)pIpForwardTable->table[i].dwForwardIfIndex;
        stream << (quint32)pIpForwardTable->table[i].dwForwardType;    // Old field name uses DWORD type.
        stream << (quint32)pIpForwardTable->table[i].dwForwardProto;   // Old field name uses DWORD type.
        stream << (quint32)pIpForwardTable->table[i].dwForwardAge;
        stream << (quint32)pIpForwardTable->table[i].dwForwardNextHopAS;
        stream << (quint32)pIpForwardTable->table[i].dwForwardMetric1;
        stream << (quint32)pIpForwardTable->table[i].dwForwardMetric2;
        stream << (quint32)pIpForwardTable->table[i].dwForwardMetric3;
        stream << (quint32)pIpForwardTable->table[i].dwForwardMetric4;
        stream << (quint32)pIpForwardTable->table[i].dwForwardMetric5;
    }
}

void NetHelper::Restore(std::string filename)
{
    QFile file(filename.c_str());
    file.open(QIODevice::ReadOnly);
    QDataStream stream(&file);

    quint32 Cnt;
    stream >> Cnt;
    for (unsigned int i = 0; i < Cnt; i++)
    {
        MIB_IPFORWARDROW row;
        memset(&row, 0, sizeof(row));
        stream >> (quint32&)row.dwForwardDest;
        stream >> (quint32&)row.dwForwardMask;
        stream >> (quint32&)row.dwForwardPolicy;
        stream >> (quint32&)row.dwForwardNextHop;
        stream >> (quint32&)row.dwForwardIfIndex;
        stream >> (quint32&)row.dwForwardType;    // Old field name uses DWORD type.
        stream >> (quint32&)row.dwForwardProto;   // Old field name uses DWORD type.
        stream >> (quint32&)row.dwForwardAge;
        stream >> (quint32&)row.dwForwardNextHopAS;
        stream >> (quint32&)row.dwForwardMetric1;
        stream >> (quint32&)row.dwForwardMetric2;
        stream >> (quint32&)row.dwForwardMetric3;
        stream >> (quint32&)row.dwForwardMetric4;
        stream >> (quint32&)row.dwForwardMetric5;

        SetIpForwardEntry(&row);
    }
}

void NetHelper::Clear()
{
    ULONG dwSize = 0;
    PMIB_IPFORWARDTABLE pIpForwardTable = NULL;
    GetIpForwardTable(pIpForwardTable, &dwSize, 0);
    pIpForwardTable = (PMIB_IPFORWARDTABLE)malloc(dwSize);
    memset(pIpForwardTable, 0, dwSize);

    GetIpForwardTable(pIpForwardTable, &dwSize, FALSE);
    for (unsigned int i = 0; i < pIpForwardTable->dwNumEntries; i++)
    {
        DeleteIpForwardEntry(&pIpForwardTable->table[i]);
    }
}
