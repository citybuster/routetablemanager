#ifndef ROUTETABLEMODEL_H
#define ROUTETABLEMODEL_H

#include <QAbstractItemModel>

#include <map>
#include <vector>
#include <functional>

#include <boost/shared_ptr.hpp>

#include "nethelper.h"

enum RootIndexType { RouteTableIndex, ForwardTypeIndex = 100, ForwardProtoIndex, InterfaceIndex };
enum ImpStatus { NONE = 0, StartImp, EndImp };

class XRouteTableModel : public QAbstractItemModel
{
    typedef std::map<int, QVariant>     Cell;
    typedef std::vector<Cell>           Row;
    typedef std::vector<Row>            Table;
    typedef boost::shared_ptr<Row>      SpRow;
    typedef boost::shared_ptr<Table>    SpTable;
    typedef std::pair<SpRow, SpTable>   SpDataPair;

    Q_OBJECT
public:
    explicit XRouteTableModel(RootIndexType type, QObject *parent = 0);

    // QAbstractItemModel interface
public:
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation = Qt::Horizontal, int role = Qt::DisplayRole) const;
    QModelIndex GetItemIndex(Item &item);
    void RefurbishOpt(std::function<void()> opt);
    void Refurbish();

protected:
    void InitValue();
    void InitIntf();
    void MonitorChange();

signals:
    void UpdateRouteTable(SpDataPair *routeTable);
    void UpdateInterface(Row *pRow);
    void LoadProgress(int range, int pos);

public slots:
    void RefurbishRouteTable(SpDataPair *routeTable);
    void RefurbishInterface(Row *pRow);

private:
    SpRow           m_pHeader;
    SpTable         m_pData;
    SpRow           m_pInterface;
    RootIndexType   m_RootType;
    QMutex          m_Mutex;
    ImpStatus       m_eBatchOpting;
};

#endif // ROUTETABLEMODEL_H
