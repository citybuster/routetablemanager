#ifndef __UTILITY_INL__
#define __UTILITY_INL__

#include <boost/lexical_cast.hpp>

template <class T>
T XUtility::strtoV(std::string str)
{
    T ret = 0;
    try
    {
        ret = boost::lexical_cast<T>(str);
    }
    catch (boost::bad_lexical_cast & e)
    {
        e;
    }
    return ret;
}

template <class T>
std::string XUtility::Vtostr(T l)
{
    std::string ret;
    try
    {
        ret = boost::lexical_cast<std::string>(l);
    }
    catch (boost::bad_lexical_cast & e)
    {
        e;
    }
    return ret;
}

template <class T>
T XUtility::strtoEnum(std::string str)
{
    unsigned int ret = 0;
    try
    {
        ret = boost::lexical_cast<unsigned int>(str);
    }
    catch (boost::bad_lexical_cast & e)
    {
        e;
    }
    return (T)ret;
}

#endif // __UTILITY_INL__
